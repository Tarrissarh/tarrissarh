"use strict";
$(document).ready(function () {
    let translate = {
        'en' : {
            'send'      :   'Сообщение отправлено! В ближайшее время отвечу.',
            'ajax'      :   'Sorry, there was an error. Try later!',
            'email'     :   'Email address is not entered correctly!',
            'phone'     :   'Phone number entered incorrectly!',
            'subject'   :   'The subject of the message is incorrectly entered!',
            'name'      :   'The name is entered incorrectly!',
        },
        'ru' : {
            'send'      :   'Message sent! In the near future I will answer.',
            'ajax'      :   'Извините, произошла ошибка. Попробуйте позже!',
            'email'     :   'Адрес электронной почты введен неправильно!',
            'phone'     :   'Номер телефона введен неправильно!',
            'subject'   :   'Тема сообщения неправильно введена!',
            'name'      :   'Имя введено неправильно!',
        }
    };

    function validation() {
        let subject =   validSubject();
        let name    =   validName();
        let mail    =   validMail();
        let phone   =   validPhone();

        if (subject) {
            if (name) {
                if (mail) {
                    if (phone) {
                        return true;
                    }
                }
            }
        }
    }

    function validSubject() {
        const msg   =   $('#error-subject');
        let re      =   /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s0-9\-\_\:\,\.]*$/;
        let subject =   $('#contact_form_subject').val();
        let valid   =   re.test(subject);

        msg.text('').css({'padding' : '0px'});

        if (!valid) {
            msg.text(translate[locale]['subject']).css({
                'border-radius' :   '5px',
                'padding'       :   '5px'
            });
        }

        return valid;
    }

    function validName() {
        const msg   =   $('#error-name');
        let re      =   /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]*$/;
        let name    =   $('#contact_form_name').val();
        let valid   =   re.test(name);

        msg.text('').css({'padding' : '0px'});

        if (!valid) {
            msg.text(translate[locale]['name']).css({
                'border-radius' :   '5px',
                'padding'       :   '5px'
            });
        }

        return valid;
    }

    function validMail() {
        const msg   =   $('#error-email');
        let re      =   /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
        let email   =   $('#contact_form_email').val();
        let valid   =   re.test(email);

        msg.text('').css({'padding' : '0px'});

        if (!valid) {
            msg.text(translate[locale]['email']).css({
                'border-radius' :   '5px',
                'padding'       :   '5px'
            });
        }

        return valid;
    }

    function validPhone() {
        const msg   =   $('#error-phone');
        let re      =   /^[\+\d][\d\(\)\ -]{4,18}\d$/;
        let phone   =   $('#contact_form_phone').val();
        let valid   =   re.test(phone);

        msg.text('').css({'padding' : '0px'});

        if (!valid) {
            msg.text(translate[locale]['phone']).css({
                'border-radius' :   '5px',
                'padding'       :   '5px'
            });
        }

        return valid;
    }

    $('form').on('submit', function (e) {
        e.preventDefault();

        if (validation()) {
            $.ajax({
                type    :   $(this).attr('method'),
                url     :   $(this).attr('action'),
                data    :   $(this).serialize(),
                success :   function (result) {
                    console.log(result);
                    if (result) {
                        alert(translate[locale]['send']);
                    } else {
                        alert(translate[locale]['ajax']);
                    }
                },
                error   :   function (result) {
                    alert(translate[locale]['ajax']);
                    console.log(result);
                }
            });
        }
    });
});