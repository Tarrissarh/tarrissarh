"use strict";

function test() {return false}

// запрет на перетаскивание
document.ondragstart = test;

// запрет на выделение элементов страницы
document.onselectstart = test;

// запрет на выведение контекстного меню
document.oncontextmenu = test;