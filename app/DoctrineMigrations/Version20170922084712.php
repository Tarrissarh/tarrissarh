<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170922084712 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, \'admin\', \'admin\', \'test@mail.ru\', \'test@mail.ru\', 1, NULL, \'$2y$13$MD.yRrGogsnCCqo85CQgPu.zT/tXxHRUwoHmP75UZKZq/ctZyqaHe\', \'2017-09-21 13:41:22\', NULL, NULL, \'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}\');');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
