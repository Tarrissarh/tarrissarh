<?php

namespace AppBundle\Services;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale = 'en')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request    =   $event->getRequest();
        $domain     =   explode('.', $request->getHttpHost());

        if ($domain[0] != $request->getLocale() && strlen($domain[0]) == 2) {
            $request->setLocale($domain[0]);
            $request->getSession()->set('_locale', $domain[0]);
        } else {
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }

        if (!$request->hasPreviousSession()) {
            return;
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
        );
    }
}