<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\Finder\Finder;

class Functions
{
    private $container;
    private $lang = [
        'English' => 'en',
        'Русский' => 'ru'
    ];

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Get all languages
     * @return array
     */
    public function getLang():array
    {
        return $this->lang;
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route         The name of the route
     * @param mixed  $parameters    An array of parameters
     * @param int    $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * Returns a RedirectResponse to the given URL.
     *
     * @param string $url    The URL to redirect to
     * @param int    $status The status code to use for the Response
     *
     * @return RedirectResponse
     */
    public function redirect($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * Generator breadcrums
     * @param   string  $action Page
     * @param   string  $type   Data type
     * @return  array
     */
    public function breadcrums(string $action = 'dashboard', string $type = ''):array
    {
        $translator =   $this->container->get('translator');
        $breadcrums[0] = [
            'name'      =>  $translator->trans('admin.dashboard'),
            'active'    =>  true
        ];

        if ($action === 'list') {
            $breadcrums[0]['url']       =   $this->generateUrl('dashboard');
            $breadcrums[0]['active']    =   false;

            $breadcrums[1] = [
                'name'      =>  $translator->trans('admin.elements.many.' . $type),
                'active'    =>  true
            ];
        } elseif ($action === 'new' || $action === 'edit') {
            $breadcrums[0]['url']       =   $this->generateUrl('dashboard');
            $breadcrums[0]['active']    =   false;

            $breadcrums[1] = [
                'name'      =>  $translator->trans('admin.elements.one.' . $type),
                'url'       =>  $this->generateUrl('list_' . $type),
                'active'    =>  false
            ];

            $breadcrums[2] = [
                'name'      =>  $translator->trans('admin.' . $action . '.' . $type),
                'active'    =>  true
            ];
        } elseif ($action === 'robots') {
            $breadcrums[0]['url']       =   $this->generateUrl('dashboard');
            $breadcrums[0]['active']    =   false;

            $breadcrums[1] = [
                'name'      =>  'Robots.txt',
                'active'    =>  true
            ];
        } elseif ($action === 'feedback') {
            $breadcrums[0]['url']       =   $this->generateUrl('dashboard');
            $breadcrums[0]['active']    =   false;

            $breadcrums[1] = [
                'name'      =>  $translator->trans('admin.feedback'),
                'active'    =>  true
            ];
        }

        return $breadcrums;
    }

    /**
     * Get template for page and article
     * @param   string  $type   Data type
     * @return  array
     */
    public function getTemplate(string $type = ''):array
    {
        $finder =   new Finder();
        $files  =   [];

        $finder->files()->in('../app/Resources/views/site/' . $type);

        foreach ($finder as $file) {
            $templateFullName                       =   explode('.', $file->getRelativePathname());
            $files[ucfirst($templateFullName[0])]   =   $templateFullName[0];
        }

        return $files;
    }

    /**
     * Get all data by type
     * @param   string  $type       Data type
     * @param   string  $typeOF     Return type data
     * @return  array
     */
    public function getAllData(string $type = 'article', string $typeOF = 'object'):array
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        // Find all meta tags for page or article
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:' . ucfirst($type));
        $elements   =   $repository->findBy(['locale' => $request->getLocale()]);

        if ($typeOF === 'array') {
            $allElem   =   [];

            foreach ($elements as $elem) {
                $allElem[$elem->getTitle()] = $elem->getId();
            }

            $elements = $allElem;
        }

        return $elements;
    }

    /**
     * Get all data by type without element with $id
     * @param   string  $type       Data type
     * @param   string  $typeOF     Return type data
     * @param   int     $id         ID element
     * @return  array
     */
    public function getAllDataNotSelf(string $type = 'article', int $id = 1, string $typeOF = 'object'):array
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        // Find all meta tags for page or article
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:' . ucfirst($type));
        $elements   =   $repository->findWithOutOne($id, $request->getLocale());

        if ($typeOF === 'array') {
            $allElem   =   [];

            foreach ($elements as $elem) {
                $allElem[$elem->getTitle()] = $elem->getId();
            }

            $elements = $allElem;
        }

        return $elements;
    }

    /**
     * Remove all meta tags
     * @param int $id       ID element
     * @param string $type  Type element
     */
    public function removeMeta(int $id = 1, string $type = 'article')
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        // Find all meta tags for page or article
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:Meta');
        $entities   =   $repository->findBy([
            'typeId'    =>  $id,
            'type'      =>  $type,
            'locale'    =>  $request->getLocale()
        ]);

        // If entity exist, remove all
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                $em->remove($entity);
                $em->flush();
            }
        }
    }

    /**
     * Remove sub blocks
     * @param int $id   ID parent block
     */
    public function removeSubBlocks(int $id = 1)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        // Find all sub blocks
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:Block');
        $entities   =   $repository->findBy([
            'parent'    =>  $id,
            'locale'    =>  $request->getLocale()
        ]);

        // If entity exist, remove all
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                $this->removeSubBlocks($entity->getId());
                $em->remove($entity);
                $em->flush();
            }
        }
    }

    /**
     * Remove blocks
     * @param int $id       ID element
     * @param string $type  Type element
     */
    public function removeBlocks(int $id = 1, string $type = 'page')
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        // Find all sub blocks
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:Block');
        $entities   =   $repository->findBy([
            'typeId'    =>  $id,
            'type'      =>  $type,
            'locale'    =>  $request->getLocale()
        ]);

        // If entity exist, remove all
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                $em->remove($entity);
                $em->flush();
            }
        }
    }

    /**
     * Remove sub category
     * @param int $id   ID parent category
     */
    public function removeSubCategory(int $id = 1)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        // Find all sub category
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:Category');
        $entities   =   $repository->findBy([
            'parent'    =>  $id,
            'locale'    =>  $request->getLocale()
        ]);

        // If entity exist, remove all
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                $this->removeSubCategory($entity->getId());
                $this->removeArticlesAndProjectsByCategory($entity);
                $this->removeMeta($entity->getId(), 'category');
                $em->remove($entity);
                $em->flush();
            }
        }
    }

    /**
     * Remove articles and project with meta tags by category
     * @param object $category  Category
     */
    public function removeArticlesAndProjectsByCategory($category)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        // Find articles by category
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:Article');
        $articles   =   $repository->findBy([
            'category'  =>  $category,
            'locale'    =>  $request->getLocale()
        ]);

        $repository =   $em->getRepository('AppBundle:Projects');
        $projects   =   $repository->findBy([
            'category'  =>  $category,
            'locale'    =>  $request->getLocale()
        ]);

        // If entity exist, remove all
        if (!empty($articles)) {
            foreach ($articles as $article) {
                $this->removeMeta($article->getId(), 'article');
                $em->remove($article);
                $em->flush();
            }
        }

        if (!empty($projects)) {
            $fileUploader = $this->container->get('app.uploader');

            $fileUploader->setDir('projects/images/');

            foreach ($projects as $project) {
                $this->removeMeta($project->getId(), 'project');
                $fileUploader->remove($project->getUrl());
                $em->remove($project);
                $em->flush();
            }
        }
    }

    /**
     * Remove articles, projects, category and meta tags
     * @param string $locale Locale
     */
    public function removeAllCategory(string $locale = 'en')
    {
        // Find articles by category
        $em         =   $this->container->get('doctrine.orm.entity_manager');
        $repository =   $em->getRepository('AppBundle:Category');
        $entities   =   $repository->findBy(['locale' => $locale]);

        // If entity exist, remove all
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                // Find and remove all article and meta tag
                $repArt =   $em->getRepository('AppBundle:Article');
                $enArts =   $repArt->findBy(['locale' => $locale]);

                foreach ($enArts as $article) {
                    $this->removeMeta($article->getId(), 'article');
                    $em->remove($article);
                    $em->flush();
                }

                // Find and remove all project, project images and meta tags
                $repPro         =   $em->getRepository('AppBundle:Project');
                $enPro          =   $repPro->findBy(['locale' => $locale]);
                $fileUploader   =   $this->container->get('app.uploader');

                $fileUploader->setDir('projects/images/');

                foreach ($enPro as $project) {
                    $this->removeMeta($project->getId(), 'project');

                    $fileUploader->remove($project->getUrl());

                    $em->remove($project);
                    $em->flush();
                }

                $this->removeMeta($entity->getId(), 'category');

                $em->remove($entity);
                $em->flush();
            }
        }
    }
}