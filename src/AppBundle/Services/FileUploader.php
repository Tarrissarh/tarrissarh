<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;

class FileUploader
{
    private $targetDir;
    private $image = '';

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function upload(UploadedFile $file)
    {
        $fileName       =   md5(uniqid()) . '.' . $file->guessExtension();
        $this->image    =   $this->getTargetDir() . $fileName;

        $file->move($this->getTargetDir(), $fileName);
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }

    public function setDir($dir)
    {
        $this->targetDir .= $dir;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function remove($path)
    {
        $fs = new Filesystem();
        $fs->remove(['file' => $path]);
    }
}