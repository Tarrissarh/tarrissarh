<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Form\Extension\Core\Type\{
    TextType,
    HiddenType,
    ChoiceType,
    TextareaType,
    SubmitType
};

class MetricType extends AbstractType
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $functions  =   $this->container->get('app.functions');
        $translator =   $this->container->get('translator');

        $builder
            ->add('id', HiddenType::class, [
                'label'     =>  false,
                'mapped'    =>  false
            ])
            ->add('typeId', ChoiceType::class, [
                'label'     =>  $translator->trans('admin.form.select.type_id'),
                'required'  =>  true,
                'choices'   =>  [$translator->trans('admin.elements.all') => 0],
                'attr'      =>  ['class' => 'form-control']
            ])
            ->add('type', ChoiceType::class, [
                'label'     =>  $translator->trans('admin.form.select.type_id'),
                'required'  =>  true,
                'choices'   =>  [
                    $translator->trans('admin.elements.all')            =>  'all',
                    $translator->trans('admin.elements.one.page')       =>  'page',
                    $translator->trans('admin.elements.one.category')   =>  'category',
                    $translator->trans('admin.elements.one.article')    =>  'article',
                ],
                'attr'      =>  ['class' => 'form-control']
            ])
            ->add('name', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.name'),
                ]
            ])
            ->add('content', TextareaType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.content'),
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' =>  $translator->trans('for_all.save'),
                'attr'  =>  ['class' => 'btn btn-success']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_metric_type';
    }
}
