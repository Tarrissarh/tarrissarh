<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Form\Extension\Core\Type\{
    TextType,
    HiddenType,
    ChoiceType,
    TextareaType,
    DateTimeType,
    SubmitType
};

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class ArticleType extends AbstractType
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $functions  =   $this->container->get('app.functions');
        $translator =   $this->container->get('translator');
        $id         =   $options['data']->getId();

        if (!is_null($options['data']->getCreated())) {
            $created = $options['data']->getCreated();
        } else {
            $created = new \DateTime();
        }

        if (is_null($options['data']->getStatus())) {
            $status = 1;
        } else {
            $status = $options['data']->getStatus();
        }

        if (is_null($options['data']->getTemplate())) {
            $template = 1;
        } else {
            $template = $options['data']->getTemplate();
        }

        if (!is_null($id)) {
            $category = $functions->getAllDataNotSelf('category', $id);
        } else {
            $category = $functions->getAllData('category');
        }

        $builder
            ->add('id', HiddenType::class, [
                'label'     =>  false,
                'mapped'    =>  false
            ])
            ->add('locale', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'choices'       =>  $functions->getLang(),
                'placeholder'   =>  $translator->trans('admin.form.select.locale'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('url', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.url')
                ]
            ])
            ->add('category', EntityType::class, [
                'class'         =>  'AppBundle:Category',
                'label'         =>  false,
                'required'      =>  false,
                'choice_label'  =>  'title',
                'choices'       =>  $category,
                'placeholder'   =>  $translator->trans('admin.form.select.category'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('template', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'data'          =>  $template,
                'placeholder'   =>  $translator->trans('admin.form.select.template'),
                'choices'       =>  $functions->getTemplate('article'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('title', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.title')
                ]
            ])
            ->add('summary', TextareaType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.summary')
                ]
            ])
            ->add('content', CKEditorType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'config'    =>  [
                    'filebrowserBrowseRoute'            =>  'elfinder',
                    'filebrowserBrowseRouteParameters'  =>  [
                        'instance'      =>  'default',
                        'homeFolder'    =>  ''
                    ]
                ],
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.content')
                ]
            ])
            ->add('created', DateTimeType::class, [
                'label'     =>  $translator->trans('admin.form.created'),
                'widget'    =>  'single_text',
                'required'  =>  true,
                'format'    =>  'YYYY-MM-dd HH:mm:ss',
                'data'      =>  $created,
                'attr'      =>  [
                    'class'             =>  'form-control',
                    'data-date-format'  =>  'YYYY-MM-dd HH:mm:ss',
                    'readonly'          =>  true
                ]
            ])
            ->add('updated', DateTimeType::class, [
                'label'     =>  $translator->trans('admin.form.updated'),
                'widget'    =>  'single_text',
                'required'  =>  true,
                'format'    =>  'YYYY-MM-dd HH:mm:ss',
                'data'      =>  new \DateTime(),
                'attr'      =>  [
                    'class'             =>  'form-control',
                    'data-date-format'  =>  'YYYY-MM-dd HH:mm:ss',
                    'readonly'          =>  true
                ]
            ])
            ->add('status', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'data'          =>  $status,
                'placeholder'   =>  $translator->trans('admin.form.select.status'),
                'choices'       =>  [$translator->trans('admin.status.enabled') => 1, $translator->trans('admin.status.disabled') => 0],
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('save', SubmitType::class, [
                'label' =>  $translator->trans('for_all.save'),
                'attr'  =>  ['class' => 'btn btn-success']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_article_type';
    }
}
