<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Form\Extension\Core\Type\{
    TextType,
    HiddenType,
    ChoiceType,
    TextareaType,
    SubmitType
};

class MetaType extends AbstractType
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $functions  =   $this->container->get('app.functions');
        $translator =   $this->container->get('translator');

        $builder
            ->add('id', HiddenType::class, [
                'label'     =>  false,
                'mapped'    =>  false
            ])
            ->add('locale', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'choices'       =>  $functions->getLang(),
                'placeholder'   =>  $translator->trans('admin.form.select.locale'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('typeId', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'choices'       =>  $functions->getAllData('page', 'array'),
                'placeholder'   =>  $translator->trans('admin.form.select.type_id'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('type', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'choices'       =>  [
                    $translator->trans('admin.elements.one.page')       =>  'page',
                    $translator->trans('admin.elements.one.category')   =>  'category',
                    $translator->trans('admin.elements.one.article')    =>  'article',
                ],
                'placeholder'   =>  $translator->trans('admin.form.select.type'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('name', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.name'),
                ]
            ])
            ->add('content', TextareaType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.content'),
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' =>  $translator->trans('for_all.save'),
                'attr'  =>  ['class' => 'btn btn-success']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_meta_type';
    }
}
