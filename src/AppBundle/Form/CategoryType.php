<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Form\Extension\Core\Type\{
    TextType,
    HiddenType,
    ChoiceType,
    TextareaType,
    SubmitType
};

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CategoryType extends AbstractType
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $functions  =   $this->container->get('app.functions');
        $translator =   $this->container->get('translator');
        $id         =   $options['data']->getId();

        if (!is_null($id)) {
            $category = $functions->getAllDataNotSelf('category', $id);
        } else {
            $category = $functions->getAllData('category');
        }

        if (is_null($options['data']->getStatus())) {
            $status = 1;
        } else {
            $status = $options['data']->getStatus();
        }

        $builder
            ->add('id', HiddenType::class, [
                'label'     =>  false,
                'mapped'    =>  false
            ])
            ->add('parent', EntityType::class, [
                'label'         =>  false,
                'required'      =>  false,
                'choice_label'  =>  'title',
                'class'         =>  'AppBundle:Category',
                'choices'       =>  $category,
                'placeholder'   =>  $translator->trans('admin.form.select.category'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('locale', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'choices'       =>  $functions->getLang(),
                'placeholder'   =>  $translator->trans('admin.form.select.locale'),
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('url', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.url')
                ]
            ])
            ->add('title', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.title')
                ]
            ])
            ->add('description', TextareaType::class, [
                'label'     =>  false,
                'required'  =>  false,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.description')
                ]
            ])
            ->add('status', ChoiceType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'data'          =>  $status,
                'placeholder'   =>  $translator->trans('admin.form.select.status'),
                'choices'       =>  [$translator->trans('admin.status.enabled') => 1, $translator->trans('admin.status.disabled') => 0],
                'attr'          =>  ['class' => 'form-control']
            ])
            ->add('save', SubmitType::class, [
                'label' =>  $translator->trans('for_all.save'),
                'attr'  =>  ['class' => 'btn btn-success']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_category_type';
    }
}
