<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Form\Extension\Core\Type\{
    EmailType,
    TextType,
    TextareaType,
    SubmitType
};

class ContactType extends AbstractType
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container    =   $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator =   $this->container->get('translator');

        $builder
            ->add('name', TextType::class, [
                'label'         =>  false,
                'required'      =>  true,
                'attr'          =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.name'),
                    'maxlength'     =>  255
                ]
            ])
            ->add('subject', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.subject'),
                    'maxlength'     =>  255
                ]
            ])
            ->add('phone', TextType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.phone'),
                    'maxlength'     =>  18
                ]
            ])
            ->add('email', EmailType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.email'),
                    'maxlength'     =>  255
                ]
            ])
            ->add('content', TextareaType::class, [
                'label'     =>  false,
                'required'  =>  true,
                'attr'      =>  [
                    'class'         =>  'form-control',
                    'placeholder'   =>  $translator->trans('admin.form.enter.content'),
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' =>  $translator->trans('for_all.send'),
                'attr'  =>  ['class' => 'btn btn-primary btn-xl js-scroll-trigger pointer']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return 'contact_form';
    }
}
