<?php

namespace AppBundle\Repository;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get category without one
     *
     * @param   int     $id     ID category
     * @param   string  $locale Locale
     * @return  array
     */
    public function findWithOutOne(int $id = 1, string $locale = 'en')
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $q  = $qb->select(array('c'))
            ->from('AppBundle:Category', 'c')
            ->where('c.id != :identifier and c.locale = :locale')
            ->setParameters([
                'identifier'    =>  $id,
                'locale'        =>  $locale
            ])
            ->getQuery();

        return $q->getResult();
    }

    /* findAll sort */
    public function findAll()
    {
        return $this->findBy([], ['id' => 'ASC']);
    }
}
