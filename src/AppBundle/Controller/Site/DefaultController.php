<?php

namespace AppBundle\Controller\Site;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Feedback;

use AppBundle\Form\ContactType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param   object  Request $request
     * @return  string
     */
    public function indexAction(Request $request)
    {
        // Get homepage
        $attr       =   ['env' => $this->get('kernel')->getEnvironment()];
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Page');
        $page       =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'name'      =>  'homepage',
            'status'    =>  1
        ], ['id' => 'ASC']);

        if (!empty($page)) {
            // Get blocks on page
            $homepage   =   $page[0];
            $repository =   $doctrine->getRepository('AppBundle:Block');

            // Get header for homepage
            $header = $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'name'      =>  'header-homepage',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($header)) {
                $h = $header[0];
            } else {
                $h = [];
            }

            // Get about for homepage
            $about = $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'name'      =>  'about',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($about)) {
                $a = $about[0];
            } else {
                $a = [];
            }

            // Get contact for homepage
            $contact = $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'name'      =>  'contact'
            ], ['id' => 'ASC']);

            if (!empty($contact)) {
                $c = $contact[0];
            } else {
                $c = [];
            }

            // Get service for homepage
            $service = $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'name'      =>  'services',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($service)) {
                // Get child service for homepage
                $svChild = $repository->findBy([
                    'locale'    =>  $request->getLocale(),
                    'parent'    =>  $service[0]->getId(),
                    'status'    =>  1
                ], ['id' => 'ASC']);
            } else {
                $svChild = [];
            }

            if (!empty($service)) {
                $services['parent'] = $service[0];
            } else {
                $services['parent'] = [];
            }

            $services['child'] = $svChild;

            // Get projects for homepage
            $repository =   $doctrine->getRepository('AppBundle:Project');
            $projects   =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'status'    =>  1
            ], ['id' => 'ASC']);

            $form = $this->createForm(ContactType::class, null, [
                'action'    =>  $this->generateUrl('sendEmailAjax'),
                'method'    =>  'POST',
                'attr'      =>  [
                    'id'    =>  'contactForm',
                    'name'  =>  'sentMessage'
                ]
            ]);

            $attr = array_merge($attr, [
                'page'      =>  $homepage,
                'blocks'    =>  [
                    'header'    =>  $h,
                    'about'     =>  $a,
                    'services'  =>  $services,
                    'contact'   =>  $c,
                ],
                'projects'  =>  $projects,
                'form'      =>  $form->createView(),
            ]);

            $seo = $this->getSeoAndOptions($request->getLocale(), [
                'type'      =>  'page',
                'typeId'    =>  $homepage->getId()
            ], [
                'type'      =>  'all',
                'typeId'    =>  0
            ]);

            $attr = array_merge($attr, $seo);
        } else {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('site/page/' . $homepage->getTemplate() . '.html.twig', $attr);
    }

    /**
     * @Route("/send_email_ajax", name="sendEmailAjax")
     * @param   object  Request $request
     * @return  string
     */
    public function sendEmail(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['message' => 'You can access this only using Ajax!'], 400);
        }

        $data = array_merge($request->request->get('contact_form'), [
            'locale'    =>  $request->getLocale(),
            'sendDate'  =>  new \DateTime()
        ]);

        if ($data['_token'] !== $this->get('security.csrf.token_manager')->getToken('contact_form')->getValue()) {
            return new JsonResponse(['message' => 'Invalid CSRF token!'], 400);
        }

        $contact = new Feedback();

        // Set params
        $contact->setSubject($data['subject']);
        $contact->setName($data['name']);
        $contact->setContent($data['content']);
        $contact->setEmail($data['email']);
        $contact->setPhone($data['phone']);
        $contact->setLocale($data['locale']);
        $contact->setSendDate($data['sendDate']);

        // Get my email
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Option');
        $option     =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'name'      =>  'email'
        ], ['id' => 'ASC']);

        // Send message
        if (!empty($option[0]->getContent())) {
            $mailer_host        =   $this->getParameter('mailer_host');
            $mailer_port        =   $this->getParameter('mailer_port');
            $mailer_transport   =   $this->getParameter('mailer_transport');
            $mailer_security    =   $this->getParameter('mailer_security');
            $mailer_user        =   $this->getParameter('mailer_user');
            $mailer_password    =   $this->getParameter('mailer_password');
            $host               =   $mailer_transport . '.' . $mailer_host;
            $translator         =   $this->container->get('translator');

            $transport = \Swift_SmtpTransport::newInstance($host, $mailer_port, $mailer_security)
                ->setUsername($mailer_user)
                ->setPassword($mailer_password);

            $mailer = \Swift_Mailer::newInstance($transport);

            $message = \Swift_Message::newInstance("Message by Tarrissarh: " . $data["subject"])
                ->setFrom($data["email"], $data["name"])
                ->setTo($option[0]->getContent(), $translator->trans('for_all.my_name'))
                ->setBody('Phone: ' . $data["phone"] . '<br>' . $data["content"]);

            if ($mailer->send($message)) {
                // Insert into DB
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush();

                return true;
            }

            return false;
        }
    }

    /**
     * @Route("/projects", name="projects")
     * @param   object  Request $request
     * @return  string
     */
    public function projectsAction(Request $request)
    {
        // Get project page
        $attr       =   ['env' => $this->get('kernel')->getEnvironment()];
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Page');
        $page       =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'name'      =>  'projects',
            'status'    =>  1
        ], ['id' => 'ASC']);

        if (!is_null($page)) {
            // Get header block for project page
            $project    =   $page[0];
            $repository =   $doctrine->getRepository('AppBundle:Block');
            $header     =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'name'      =>  'header-projects',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($header)) {
                $h = $header[0];
            } else {
                $h = [];
            }

            // Get projects
            $repository =   $doctrine->getRepository('AppBundle:Project');
            $projects       =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'status'    =>  1
            ], ['id' => 'ASC']);

            $attr = array_merge($attr, [
                'page'      =>  $project,
                'projects'  =>  $projects,
                'blocks'    =>  ['header' => $h]
            ]);

            $seo = $this->getSeoAndOptions($request->getLocale(), [
                'type'      =>  'page',
                'typeId'    =>  $project->getId()
            ], [
                'type'      =>  'all',
                'typeId'    =>  0
            ]);

            $attr = array_merge($attr, $seo);
        } else {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('site/page/' . $project->getTemplate() . '.html.twig', $attr);
    }

    /**
     * @Route("/articles", name="articles")
     * @param   object  Request $request
     * @return  string
     */
    public function articlesAction(Request $request)
    {
        // Get article page
        $attr       =   ['env' => $this->get('kernel')->getEnvironment()];
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Page');
        $article    =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'name'      =>  'articles',
            'status'    =>  1
        ], ['id' => 'ASC']);

        if (!empty($article)) {
            // Get articles
            $page       =   $article[0];
            $repository =   $doctrine->getRepository('AppBundle:Article');
            $articles   =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'status'    =>  1
            ], ['id' => 'ASC']);

            // Get header block for article page
            $repository =   $doctrine->getRepository('AppBundle:Block');
            $header     =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'name'      =>  'header-articles',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($header)) {
                $h = $header[0];
            } else {
                $h = [];
            }

            $attr = array_merge($attr, [
                'page'      =>  $page,
                'articles'  =>  $articles,
                'blocks'    =>  ['header' => $h]
            ]);

            $seo = $this->getSeoAndOptions($request->getLocale(), [
                'type'      =>  'page',
                'typeId'    =>  $page->getId()
            ], [
                'type'      =>  'all',
                'typeId'    =>  0
            ]);

            $attr = array_merge($attr, $seo);
        } else {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('site/page/' . $page->getTemplate() . '.html.twig', $attr);
    }

    /**
     * @Route("/categories", name="categories")
     * @param   object  Request $request
     * @return  string
     */
    public function categoriesAction(Request $request)
    {
        // Get category page
        $attr       =   ['env' => $this->get('kernel')->getEnvironment()];
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Page');
        $cat        =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'name'      =>  'category',
            'status'    =>  1
        ], ['id' => 'ASC']);

        if (!empty($cat)) {
            // Get header block for category page
            $page       =   $cat[0];
            $repository =   $doctrine->getRepository('AppBundle:Block');
            $header     =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'name'      =>  'header-category',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($header)) {
                $h = $header[0];
            } else {
                $h = [];
            }

            // Get categories divided into 2 main categories: Projects and Articles, and their subcategories
            $repository     =   $doctrine->getRepository('AppBundle:Category');
            $catPortfolio   =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'url'       =>  'projects',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($catPortfolio)) {
                $portfolio          =   $catPortfolio[0];
                $catSubPortfolio    =   $repository->findBy([
                    'locale'    =>  $request->getLocale(),
                    'parent'    =>  $portfolio->getId(),
                    'status'    =>  1
                ], ['id' => 'ASC']);
            } else {
                $portfolio          =   [];
                $catSubPortfolio    =   [];
            }

            $catArticles = $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'url'       =>  'articles',
                'status'    =>  1
            ], ['id' => 'ASC']);

            if (!empty($catArticles)) {
                $articles          =   $catArticles[0];
                $catSubArticles    =   $repository->findBy([
                    'locale'    =>  $request->getLocale(),
                    'parent'    =>  $articles->getId(),
                    'status'    =>  1
                ], ['id' => 'ASC']);
            } else {
                $articles          =   [];
                $catSubArticles    =   [];
            }

            $allCategory['articles']['parent'] = $articles;
            $allCategory['articles']['child'] = $catSubArticles;
            $allCategory['portfolio']['parent'] = $portfolio;
            $allCategory['portfolio']['child'] = $catSubPortfolio;

            $attr = array_merge($attr, [
                'page'      =>  $page,
                'blocks'    =>  ['header' => $h],
                'category'  =>  $allCategory,
            ]);

            $seo = $this->getSeoAndOptions($request->getLocale(), [
                'type'      =>  'page',
                'typeId'    =>  $page->getId()
            ], [
                'type'      =>  'all',
                'typeId'    =>  0
            ]);

            $attr = array_merge($attr, $seo);
        } else {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('site/page/' . $page->getTemplate() . '.html.twig', $attr);
    }

    /**
     * @Route("/category/{cat_url}", name="category", requirements={"cat_url" : "[a-z0-9\-\_\/]+"})
     * @param   object  Request $request
     * @param   string  $cat_url            Category url
     * @return  string
     */
    public function categoryAction(Request $request, string $cat_url = '')
    {
        // Get category
        $attr       =   ['env' => $this->get('kernel')->getEnvironment()];
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Category');
        $category   =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'url'       =>  $cat_url,
            'status'    =>  1
        ], ['id' => 'ASC']);

        if (!empty($category)) {
            // Get articles with $category
            $repository =   $doctrine->getRepository('AppBundle:Article');
            $article    =   $repository->findBy([
                'locale'    =>  $request->getLocale(),
                'category'  =>  $category[0]->getId()
            ], ['id' => 'ASC']);

            $attr = array_merge($attr, [
                'articles'  =>  $article,
                'category'  =>  $category[0]
            ]);

            $seo = $this->getSeoAndOptions($request->getLocale(), [
                'type'      =>  'category',
                'typeId'    =>  $category[0]->getId()
            ], [
                'type'      =>  'all',
                'typeId'    =>  0
            ]);

            $attr = array_merge($attr, $seo);
        } else {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('site/category/main.html.twig', $attr);
    }

    /**
     * @Route("/project/{project_url}", name="project", requirements={"project_url" : "[a-z0-9\-\_\/]+"})
     * @param   object  Request $request
     * @param   string  $project_url        Project url
     * @return  string
     */
    public function projectAction(Request $request, string $project_url = '')
    {
        // Get article
        $attr       =   ['env' => $this->get('kernel')->getEnvironment()];
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Project');
        $project    =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'url'       =>  $project_url
        ], ['id' => 'ASC']);

        if (!empty($project)) {
            $attr = array_merge($attr, ['project' => $project[0]]);

            $seo = $this->getSeoAndOptions($request->getLocale(), [
                'type'      =>  'project',
                'typeId'    =>  $project[0]->getId()
            ], [
                'type'      =>  'all',
                'typeId'    =>  0
            ]);

            $attr = array_merge($attr, $seo);
        } else {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('site/project/main.html.twig', $attr);
    }

    /**
     * @Route("/article/{article_url}", name="article", requirements={"article_url" : "[a-z0-9\-\_\/]+"})
     * @param   object  Request $request
     * @param   string  $article_url        Article url
     * @return  string
     */
    public function articleAction(Request $request, string $article_url = '')
    {
        // Get article
        $attr       =   ['env' => $this->get('kernel')->getEnvironment()];
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Article');
        $article    =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'url'       =>  $article_url
        ], ['id' => 'ASC']);

        if (!empty($article)) {
            $attr = array_merge($attr, ['article' => $article[0]]);

            $seo = $this->getSeoAndOptions($request->getLocale(), [
                'type'      =>  'article',
                'typeId'    =>  $article[0]->getId()
            ], [
                'type'      =>  'all',
                'typeId'    =>  0
            ]);

            $attr = array_merge($attr, $seo);
        } else {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('site/article/main.html.twig', $attr);
    }

    /**
     * Get meta tags, options, metric
     * @param   string  $locale Locale
     * @param   array   $metric Array attributes for search
     * @param   array   $meta   Array attributes for search
     * @return  array
     */
    private function getSeoAndOptions($locale = 'en', array $metric = [], array $meta = []):array
    {
        $doctrine   =   $this->getDoctrine();
        $data       =   array();

        if (!empty($metric)) {
            // Get meta tags
            $repository             =   $doctrine->getRepository('AppBundle:Meta');
            $data['metaObjects']    =   $repository->findBy([
                'locale'    =>  $locale,
                'type'      =>  $metric['type'],
                'typeId'    =>  $metric['typeId']
            ], ['id' => 'ASC']);
        }

        if (!empty($meta)) {
            // Get metric
            $repository         =   $doctrine->getRepository('AppBundle:Metric');
            $data['metrics']    =   $repository->findBy([
                'type'      =>  $meta['type'],
                'typeId'    =>  $meta['typeId']
            ], ['id' => 'ASC']);
        }

        // Get options
        $repository         =   $doctrine->getRepository('AppBundle:Option');
        $data['options']    =   $repository->findBy(['locale' => $locale], ['id' => 'ASC']);

        return $data;
    }
}
