<?php

namespace AppBundle\Controller\Site;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SitemapController extends Controller
{
    /**
     * @Route("/sitemap.{_format}", name="sitemap", Requirements={"_format" = "xml"})
     */
    public function sitemapAction(Request $request)
    {
        $doctrine   =   $this->getDoctrine();
        $urls       =   [];
        $allData    =   [];
        $hostname   =   $request->getSchemeAndHttpHost();

        $urls[] = [
            'loc'           =>  $hostname,
            'changefreq'    =>  'weekly',
            'priority'      =>  '1.0'
        ];

        $repository =   $doctrine->getRepository('AppBundle:Page');
        $page       =   $repository->findByWithOutHomepage($request->getLocale());

        if (!empty($page)) {
            foreach ($page as $key => $item) {
                if ($item->getName() !== 'homepage') {
                    $urls[] = [
                        'loc'           =>  $hostname .  '/' . $item->getUrl(),
                        'changefreq'    =>  'weekly',
                        'priority'      =>  '1.0'
                    ];
                }
            }
        }

        $repository =   $doctrine->getRepository('AppBundle:Article');
        $article    =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'status'    =>  1
        ]);

        if (!empty($article)) {
            foreach ($article as $key => $item) {
                $urls[] = [
                    'loc'           =>  $hostname .  '/article/' . $item->getUrl(),
                    'changefreq'    =>  'weekly',
                    'priority'      =>  '1.0'
                ];
            }
        }

        $repository =   $doctrine->getRepository('AppBundle:Category');
        $category   =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'status'    =>  1
        ]);

        if (!empty($category)) {
            foreach ($category as $key => $item) {
                $urls[] = [
                    'loc'           =>  $hostname .  '/category/' . $item->getUrl(),
                    'changefreq'    =>  'weekly',
                    'priority'      =>  '1.0'
                ];
            }
        }

        $repository =   $doctrine->getRepository('AppBundle:Project');
        $project    =   $repository->findBy([
            'locale'    =>  $request->getLocale(),
            'status'    =>  1
        ]);

        if (!empty($project)) {
            foreach ($project as $key => $item) {
                $urls[] = [
                    'loc'           =>  $hostname .  '/project/' . $item->getUrl(),
                    'changefreq'    =>  'weekly',
                    'priority'      =>  '1.0'
                ];
            }
        }

        return $this->render('site/sitemap.xml.twig', ['urls' => $urls]);
    }
}
