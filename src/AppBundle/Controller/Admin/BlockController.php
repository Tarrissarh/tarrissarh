<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\{
    Entity\Block,
    Form\BlockType
};

class BlockController extends Controller
{
    private $type = 'block';
    
    /**
     * @Route("/admin/block", name="list_block")
     * @param   object  Request $request
     * @return  string
     */
    public function listAction(Request $request)
    {
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Block');
        $data       =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('list', $this->type);

        return $this->render('admin/list.html.twig', [
            'data'          =>  $data,
            'type'          =>  $this->type,
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/block/new", name="new_block")
     * @param   object  Request $request
     * @return  string
     */
    public function newAction(Request $request)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('new', $this->type);

        $page       =   new Block();
        $form       =   $this->createForm(BlockType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($page);
            $em->flush();

            return $this->redirectToRoute('list_block');
        }

        return $this->render('admin/new.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/block/edit/{id}", name="edit_block", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID block
     * @return  string
     */
    public function editAction(Request $request, int $id = 1)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('edit', $this->type);

        $repository =   $this->getDoctrine()->getRepository('AppBundle:Block');
        $page       =   $repository->findOneBy(['id' => $id]);
        $form       =   $this->createForm(BlockType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($page);
            $em->flush();

            return $this->redirectToRoute('list_block');
        }

        return $this->render('admin/edit.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/block/remove/{count}/{id}", name="remove_block")
     * @param   object  Request $request
     * @param   string  $count              Delete one or all blocks
     * @param   int     $id                 ID block
     * @return  string
     */
    public function removeAction(Request $request, string $count = 'one', int $id = 1):string
    {
        $em         =   $this->getDoctrine()->getManager();
        $repository =   $em->getRepository('AppBundle:Block');
        $functions  =   $this->get('app.functions');

        if ($count === 'one') {
            $entity = $repository->find($id);

            $functions->removeSubBlocks($entity->getId());

            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $entities = $repository->findBy(['locale' => $request->getLocale()]);

            foreach ($entities as $entity) {
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirectToRoute('list_block');
    }
}