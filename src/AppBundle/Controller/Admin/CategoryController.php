<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\{
    Entity\Category,
    Form\CategoryType
};

class CategoryController extends Controller
{
    private $type = 'category';

    /**
     * @Route("/admin/category", name="list_category")
     * @param   object  Request $request
     * @return  string
     */
    public function listAction(Request $request)
    {
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Category');
        $data       =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('list', $this->type);

        return $this->render('admin/list.html.twig', [
            'data'          =>  $data,
            'type'          =>  $this->type,
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/category/new", name="new_category")
     * @param   object  Request $request
     * @return  string
     */
    public function newAction(Request $request)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('new', $this->type);

        $category   =   new Category();
        $form       =   $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('list_category');
        }

        return $this->render('admin/new.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/category/edit/{id}", name="edit_category", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID category
     * @return  string
     */
    public function editAction(Request $request, int $id = 1)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('edit', $this->type);

        $repository =   $this->getDoctrine()->getRepository('AppBundle:Category');
        $category   =   $repository->findOneBy(['id' => $id]);
        $form       =   $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('list_category');
        }

        return $this->render('admin/edit.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/category/remove/{count}/{id}", name="remove_category")
     * @param   object  Request $request
     * @param   string  $count              Delete one or all categories
     * @param   int     $id                 ID category
     * @return  string
     */
    public function removeAction(Request $request, string $count = 'one', int $id = 1):string
    {
        $em         =   $this->getDoctrine()->getManager();
        $repository =   $em->getRepository('AppBundle:Category');
        $functions  =   $this->get('app.functions');

        if ($count === 'one') {
            $entity = $repository->find($id);

            $functions->removeMeta($id, $this->type);
            $functions->removeArticlesAndProjectsByCategory($entity);
            $functions->removeSubCategory($entity->getId());

            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $functions->removeAllCategory($request->getLocale());
        }

        return $this->redirectToRoute('list_category');
    }
}