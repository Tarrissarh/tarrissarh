<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\{
    HttpFoundation\Request,
    HttpFoundation\Response
};

use Symfony\Component\Form\Extension\Core\Type\{
    FormType,
    TextareaType,
    SubmitType
};

class DefaultController extends Controller
{
    /**
     * @Route("/admin", name="dashboard")
     * @return string
     */
    public function indexAction()
    {
        $functions  = $this->get('app.functions');
        $breadcrums = $functions->breadcrums();

        return $this->render('admin/index.html.twig', [
            'breadcrums' => $breadcrums
        ]);
    }

    /**
     * @Route("/admin/robots/{action}", name="robots", requirements={"action" : "[a-z]+"})
     * @param   object  Request $request
     * @param   string  $action             Page
     * @return  string
     */
    public function robotsAction(Request $request, string $action = 'edit')
    {
        if ($action === 'edit') {
            $functions  =   $this->get('app.functions');
            $file       =   fopen("../web/robots.txt", "r");
            $contents   =   "";

            while (!feof($file)) {
                $contents .= fgets($file) . "\n";
            }

            fclose($file);

            $formBuilder    =   $this->get('form.factory')->createBuilder(FormType::class, null, [
                'action'    =>  $this->generateUrl('robots', ['action' => 'save']),
                'method'    =>  'POST'
            ]);

            $formBuilder->add('content', TextareaType::class, [
                'label'     =>  'Content',
                'data'      =>  $contents,
                'required'  =>  true,
                'attr'      =>  [
                    'class' =>  'form-control',
                    'rows'  =>  10
                ],
            ]);

            $form = $formBuilder
                ->add('save', SubmitType::class, [
                    'label' =>  'Save',
                    'attr'  =>  ['class' => 'btn btn-success']
                ])
                ->getForm();

            return $this->render('admin/edit.html.twig', [
                'form'          =>  $form->createView(),
                'breadcrums'    =>  $functions->breadcrums('robots'),
                'type'          =>  'robots'
            ]);
        } else {
            $file = fopen("../web/robots.txt", "w");
            $formData = $request->request->get('form');

            if ($formData['_token'] !== $this->get('security.csrf.token_manager')->getToken('form')->getValue()) {
                return new JsonResponse(array('message' => 'Invalid CSRF token!'), 400);
            }

            fwrite($file, $formData['content']);
            fclose($file);

            return $this->redirect($this->generateUrl('robots', ['action' => 'edit']));
        }
    }

    /**
     * @Route("/admin/feedback", name="feedback")
     * @param   object  Request $request
     * @return  string
     */
    public function feedbackAction(Request $request)
    {
        $doctrine   =   $this->getDoctrine();
        $repository =   $doctrine->getRepository('AppBundle:Feedback');
        $feedbacks  =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);

        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('feedback');

        return $this->render('admin/feedback.html.twig', [
            'feedbacks'     =>  $feedbacks,
            'type'          =>  'feedback',
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/feedback_view/{id}", name="feedbackView", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID feedback
     * @return  string
     */
    public function feedbackviewAction(Request $request, int $id = 1)
    {
        $translator = $this->get('translator');

        if ($request->isXmlHttpRequest()) {
            $repository = $this->getDoctrine()->getRepository('AppBundle:Feedback');
            $feedback   = $repository->find($id);

            return new Response(json_encode($feedback->getContent()), 200);
        }

        return new Response($translator->trans('error.no_ajax'), 400);
    }

    /**
     * @Route("/admin/ajax_elements/{type}", name="ajax_elements")
     * @param   object  Request $request
     * @param   string  $type               Data type (page, category, article)
     * @return  string
     */
    public function elementsAction(Request $request, string $type = 'page')
    {
        $translator = $this->get('translator');

        if ($request->isXmlHttpRequest()) {
            $element = [];

            if ($type === 'all') {
                $element[$translator->trans('admin.elements.all')] = 0;
            } else {
                $repository =   $this->getDoctrine()->getRepository('AppBundle:' . ucfirst($type));
                $elements   =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);

                foreach ($elements as $e) {
                    $element[$e->getTitle()] = $e->getId();
                }
            }

            return new Response(json_encode($element), 200);
        }

        return new Response($translator->trans('error.no_ajax'), 400);
    }

    /**
     * @Route("/admin/feedback/remove/{count}/{id}", name="remove_feedback")
     * @param   string  $count              Delete one or all elements
     * @param   int     $id                 ID element
     * @return  string
     */
    public function removeAction(string $count = 'one', int $id = 1):string
    {
        $em         =   $this->getDoctrine()->getManager();
        $repository =   $em->getRepository('AppBundle:Feedback');

        if ($count === 'one') {
            $entity = $repository->find($id);

            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $entities = $repository->findAll();

            foreach ($entities as $entity) {
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirectToRoute('dashboard');
    }
}
