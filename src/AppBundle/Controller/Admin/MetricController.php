<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\{
    Entity\Metric,
    Form\MetricType
};

class MetricController extends Controller
{
    private $type = 'metric';

    /**
     * @Route("/admin/metric", name="list_metric")
     * @return  string
     */
    public function listAction()
    {
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Metric');
        $data       =   $repository->findAll();
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('list', $this->type);

        return $this->render('admin/list.html.twig', [
            'data'          =>  $data,
            'type'          =>  $this->type,
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/metric/new", name="new_metric")
     * @param   object  Request $request
     * @return  string
     */
    public function newAction(Request $request)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('new', $this->type);

        $metric     =   new Metric();
        $form       =   $this->createForm(MetricType::class, $metric);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($metric);
            $em->flush();

            return $this->redirectToRoute('list_metric');
        }

        return $this->render('admin/new.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/metric/edit/{id}", name="edit_metric", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID metric
     * @return  string
     */
    public function editAction(Request $request, int $id = 1)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('edit', $this->type);

        $repository =   $this->getDoctrine()->getRepository('AppBundle:Metric');
        $metric     =   $repository->findOneBy(['id' => $id]);
        $form       =   $this->createForm(MetricType::class, $metric);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($metric);
            $em->flush();

            return $this->redirectToRoute('list_metric');
        }

        return $this->render('admin/edit.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/metric/remove/{count}/{id}", name="remove_metric")
     * @param   string  $count  Delete one or all metrics
     * @param   int     $id     ID metric
     * @return  string
     */
    public function removeAction(string $count = 'one', int $id = 1):string
    {
        $em         =   $this->getDoctrine()->getManager();
        $repository =   $em->getRepository('AppBundle:Metric');

        if ($count === 'one') {
            $entity = $repository->find($id);
            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $entities = $repository->findAll();

            foreach ($entities as $entity) {
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirectToRoute('list_metric');
    }
}