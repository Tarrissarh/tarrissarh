<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\{
    Entity\Project,
    Form\ProjectType
};

class ProjectController extends Controller
{
    private $type = 'project';

    /**
     * @Route("/admin/project", name="list_project")
     * @param   object  Request $request
     * @return  string
     */
    public function listAction(Request $request)
    {
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Project');
        $data       =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);

        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('list', $this->type);

        return $this->render('admin/list.html.twig', [
            'data'          =>  $data,
            'type'          =>  $this->type,
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/project/new", name="new_project")
     * @param   object  Request $request
     * @return  string
     */
    public function newAction(Request $request)
    {
        $functions      =   $this->get('app.functions');
        $fileUploader   =   $this->get('app.uploader');
        $breadcrums     =   $functions->breadcrums('new', $this->type);

        $project        =   new Project();
        $form           =   $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $fileUploader->setDir('projects/images/');
            $fileUploader->upload($form->getData()->getImage());

            $project->setImage($fileUploader->getImage());

            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('list_project');
        }

        return $this->render('admin/new.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/project/edit/{id}", name="edit_project", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID project
     * @return  string
     */
    public function editAction(Request $request, int $id = 1)
    {
        $functions      =   $this->get('app.functions');
        $fileUploader   =   $this->get('app.uploader');
        $breadcrums     =   $functions->breadcrums('edit', $this->type);

        $repository     =   $this->getDoctrine()->getRepository('AppBundle:Project');
        $project        =   $repository->findOneBy(['id' => $id]);
        $form           =   $this->createForm(ProjectType::class, $project);
        $image          =   $project->getImage();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (!is_null($form->getData()->getImage())) {
                $fileUploader->setDir('projects/images/');
                $fileUploader->upload($form->getData()->getImage());
                $fileUploader->remove($image);

                $project->setImage($fileUploader->getImage());
            } else {
                $project->setImage($image);
            }

            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('list_project');
        }

        return $this->render('admin/edit.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/project/remove/{count}/{id}", name="remove_project")
     * @param   object  Request $request
     * @param   string  $count              Delete one or all projects
     * @param   int     $id                 ID project
     * @return  string
     */
    public function removeAction(Request $request, string $count = 'one', int $id = 1):string
    {
        $em             =   $this->getDoctrine()->getManager();
        $repository     =   $em->getRepository('AppBundle:Project');
        $functions      =   $this->get('app.functions');
        $fileUploader   =   $this->get('app.uploader');

        $fileUploader->setDir('projects/images/');

        if ($count === 'one') {
            $entity = $repository->find($id);

            $functions->removeMeta($id, $this->type);
            $fileUploader->remove($entity->getUrl());

            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $entities = $repository->findBy(['locale' => $request->getLocale()]);

            foreach ($entities as $entity) {
                $functions->removeMeta($entity->getId(), $this->type);
                $fileUploader->remove($entity->getUrl());
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirectToRoute('list_project');
    }
}