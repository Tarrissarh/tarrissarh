<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\{
    Entity\Article,
    Form\ArticleType
};

class ArticleController extends Controller
{
    private $type = 'article';

    /**
     * @Route("/admin/article", name="list_article")
     * @param   object  Request $request
     * @return  string
     */
    public function listAction(Request $request)
    {
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Article');
        $data       =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('list', $this->type);

        return $this->render('admin/list.html.twig', [
            'data'          =>  $data,
            'type'          =>  $this->type,
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/article/new", name="new_article")
     * @param   object  Request $request
     * @return  string
     */
    public function newAction(Request $request)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('new', $this->type);

        $article    =   new Article();
        $form       =   $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('list_article');
        }

        return $this->render('admin/new.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/article/edit/{id}", name="edit_article", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID article
     * @return  string
     */
    public function editAction(Request $request, int $id = 1)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('edit', $this->type);

        $repository =   $this->getDoctrine()->getRepository('AppBundle:Article');
        $article    =   $repository->findOneBy(['id' => $id]);
        $form       =   $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('list_article');
        }

        return $this->render('admin/edit.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/article/remove/{count}/{id}", name="remove_article")
     * @param   object  Request $request
     * @param   string  $count              Delete one or all articles
     * @param   int     $id                 ID article
     * @return  string
     */
    public function removeAction(Request $request, string $count = 'one', int $id = 1):string
    {
        $em         =   $this->getDoctrine()->getManager();
        $repository =   $em->getRepository('AppBundle:Article');
        $functions  =   $this->get('app.functions');

        if ($count === 'one') {
            $entity = $repository->find($id);

            $functions->removeMeta($id, $this->type);

            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $entities = $repository->findBy(['locale' => $request->getLocale()]);

            foreach ($entities as $entity) {
                $functions->removeMeta($entity->getId(), $this->type);
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirectToRoute('list_article');
    }
}