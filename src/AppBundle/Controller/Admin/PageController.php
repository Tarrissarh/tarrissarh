<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\{
    Entity\Page,
    Form\PageType
};

class PageController extends Controller
{
    private $type = 'page';
    
    /**
     * @Route("/admin/page", name="list_page")
     * @param   object  Request $request
     * @return  string
     */
    public function listAction(Request $request)
    {
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Page');
        $data       =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('list', $this->type);

        return $this->render('admin/list.html.twig', [
            'data'          =>  $data,
            'type'          =>  $this->type,
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/page/new", name="new_page")
     * @param   object  Request $request
     * @return  string
     */
    public function newAction(Request $request)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('new', $this->type);

        $page       =   new Page();
        $form       =   $this->createForm(PageType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($page);
            $em->flush();

            return $this->redirectToRoute('list_page');
        }

        return $this->render('admin/new.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/page/edit/{id}", name="edit_page", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID page
     * @return  string
     */
    public function editAction(Request $request, int $id = 1)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('edit', $this->type);

        $repository =   $this->getDoctrine()->getRepository('AppBundle:Page');
        $page       =   $repository->findOneBy(['id' => $id]);
        $form       =   $this->createForm(PageType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($page);
            $em->flush();

            return $this->redirectToRoute('list_page');
        }

        return $this->render('admin/edit.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/page/remove/{count}/{id}", name="remove_page")
     * @param   object  Request $request
     * @param   string  $count              Delete one or all pages
     * @param   int     $id                 ID page
     * @return  string
     */
    public function removeAction(Request $request, string $count = 'one', int $id = 1):string
    {
        $em         =   $this->getDoctrine()->getManager();
        $repository =   $em->getRepository('AppBundle:Page');
        $functions  =   $this->get('app.functions');

        if ($count === 'one') {
            $entity = $repository->find($id);

            $functions->removeMeta($id, $this->type);
            $functions->removeBlocks($id, $this->type);

            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $entities = $repository->findBy(['locale' => $request->getLocale()]);

            foreach ($entities as $entity) {
                $functions->removeMeta($entity->getId(), $this->type);
                $functions->removeBlocks($entity->getId(), $this->type);
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirectToRoute('list_page');
    }
}