<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\{
    Entity\Option,
    Form\OptionType
};

class OptionController extends Controller
{
    private $type = 'option';

    /**
     * @Route("/admin/option", name="list_option")
     * @param   object  Request $request
     * @return  string
     */
    public function listAction(Request $request)
    {
        $repository =   $this->getDoctrine()->getRepository('AppBundle:Option');
        $data       =   $repository->findBy(['locale' => $request->getLocale()], ['id' => 'ASC']);
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('list', $this->type);

        return $this->render('admin/list.html.twig', [
            'data'          =>  $data,
            'type'          =>  $this->type,
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/option/new", name="new_option")
     * @param   object  Request $request
     * @return  string
     */
    public function newAction(Request $request)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('new', $this->type);

        $option     =   new Option();
        $form       =   $this->createForm(OptionType::class, $option);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($option);
            $em->flush();

            return $this->redirectToRoute('list_option');
        }

        return $this->render('admin/new.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/option/edit/{id}", name="edit_option", requirements={"id" : "\d+"})
     * @param   object  Request $request
     * @param   int     $id                 ID option
     * @return  string
     */
    public function editAction(Request $request, int $id = 1)
    {
        $functions  =   $this->get('app.functions');
        $breadcrums =   $functions->breadcrums('edit', $this->type);

        $repository =   $this->getDoctrine()->getRepository('AppBundle:Option');
        $option     =   $repository->findOneBy(['id' => $id]);
        $form       =   $this->createForm(OptionType::class, $option);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($option);
            $em->flush();

            return $this->redirectToRoute('list_option');
        }

        return $this->render('admin/edit.html.twig', [
            'type'          =>  $this->type,
            'form'          =>  $form->createView(),
            'breadcrums'    =>  $breadcrums
        ]);
    }

    /**
     * @Route("/admin/option/remove/{count}/{id}", name="remove_option")
     * @param   object  Request $request
     * @param   string  $count              Delete one or all options
     * @param   int     $id                 ID option
     * @return  string
     */
    public function removeAction(Request $request, string $count = 'one', int $id = 1):string
    {
        $em         =   $this->getDoctrine()->getManager();
        $repository =   $em->getRepository('AppBundle:Option');

        if ($count === 'one') {
            $entity = $repository->find($id);
            $em->remove($entity);
            $em->flush();
        } else if ($count == 'all') {
            $entities = $repository->findBy(['locale' => $request->getLocale()]);

            foreach ($entities as $entity) {
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirectToRoute('list_option');
    }
}